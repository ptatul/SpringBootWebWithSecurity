package com.atul.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atul.auth.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
