package com.atul.auth.service;

import com.atul.auth.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
